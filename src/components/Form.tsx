import { useState, ChangeEvent, FormEvent, Dispatch, useEffect } from "react"
import { v4 as uuidv4 } from "uuid"

import { Operation } from "../types"
import { tipoItems } from "../data/tipoItems"
import { OperationActions, OperationState } from "../reducers/operation-reducer"

type FormProps = {
    dispatch: Dispatch<OperationActions>,
    state: OperationState
}

const initialState : Operation = {
    id: uuidv4(),
    tipoItem: 1,
    motivoPrestamo: "",
    cantidad: 0
}

export default function Form({dispatch, state} : FormProps) {

    const [operation, setOperation] = useState<Operation>(initialState)

    useEffect(() => {
        if (state.activeId) {
            //console.log(state.activeId);
            const selectOperation = state.operations.filter( stateOperation => stateOperation.id === state.activeId )[0]
            
            setOperation(selectOperation)
        }
    }, [state.activeId])

    const handleChange = (e:ChangeEvent<HTMLSelectElement> | ChangeEvent<HTMLInputElement>) => {

        const isNumberField = ['tipoItem','cantidad'].includes(e.target.id)
        //console.log(isNumberField);
        
        setOperation({
            ...operation,
            [e.target.id]: isNumberField ? +e.target.value : e.target.value
        })       
                
    }    
    
    const isValidField = () => {
        const { motivoPrestamo, cantidad } = operation
        //console.log(motivoPrestamo.trim() !== '' && cantidad > 0);
        
        return motivoPrestamo.trim() !== '' && cantidad > 0
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        dispatch({type: 'safe-operation', payload: {newOperation: operation}})

        //reseteamos el valor del formulario despues de cada carga y agregamos el id unico
        setOperation({
            ...initialState,
            id: uuidv4()
        })
    }    

    return (
        <form
        className="space-y-5 bg-gray-50 shadow p-10 rounded-md"
        onSubmit={handleSubmit}
        >
            <div>
                <label className="block text-sm font-medium leading-6 text-gray-900" htmlFor="tipoItem">Tipo de prestamo:</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <select 
                    className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" 
                    id="tipoItem"
                    value={ operation.tipoItem }
                    onChange={handleChange}
                    >
                        {tipoItems.map(tipoItems => 
                            <option 
                            key={tipoItems.id} 
                            value={tipoItems.id}
                            >
                                {tipoItems.nombre}
                            </option>
                        )}

                    </select>
                </div>
            </div>

            <div>
                <label className="block text-sm font-medium leading-6 text-gray-900" htmlFor="motivoPrestamo">Motivo prestamo:</label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <input 
                    type="text"
                    id="motivoPrestamo"
                    className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    placeholder="Ejem: Prestamo por enfermedad"
                    value={ operation.motivoPrestamo }
                    onChange={handleChange}
                    />
                </div>
            </div>

            
            <div>
                <label htmlFor="cantidad" className="block text-sm font-medium leading-6 text-gray-900">
                Cantidad del prestamo a solicitar:
                </label>
                <div className="relative mt-2 rounded-md shadow-sm">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                    <span className="text-gray-500 sm:text-sm">$</span>
                    </div>
                    <input 
                    type="number"
                    id="cantidad"
                    placeholder="ejm. $500 o $10,000"
                    className="block w-full rounded-md border-0 py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    value={ operation.cantidad }
                    onChange={handleChange} 
                    />       
                </div>
            </div>

            <input 
            type="submit"
            className="mt-10 block w-full rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 cursor-pointer disabled:opacity-10"
            value={operation.tipoItem === 1 ? 'Guardar prestamo celular' : 'Guardar prestamo en efectivo'}
            disabled={!isValidField()} 
            />

        </form>
    )

}