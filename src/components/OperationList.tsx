import { useMemo, Dispatch } from "react"
import { Operation } from "../types"
import { tipoItems } from "../data/tipoItems"
import { PencilSquareIcon, XCircleIcon } from '@heroicons/react/24/outline'
import { OperationActions } from '../reducers/operation-reducer'

type OperationListProps = {
    operations: Operation[],
    dispatch: Dispatch<OperationActions>
}

export default function OperationList( {operations, dispatch} : OperationListProps) {
    //console.log(operations);

    const tipoItemsName = useMemo(() => (tiposItem: Operation['tipoItem']) => tipoItems.map( tipo => tipo.id === tiposItem ? tipo.nombre : '')            
    , [operations])

    const isEmptyOperations = useMemo(() => operations.length === 0, [operations])

    return (
       <>
        
    <div className="bg-white py-20 sm:py-10 w-100 rounded-md" >
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-4xl sm:text-center">
          <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl p-4">Prestamos activos</h2>
          <ul role="list" className="divide-y divide-gray-100">

      { isEmptyOperations ? 
      <p className="text-center my-5">No hay prestamos cargados...</p> :
      operations.map( operation => (
        <li key={operation.id} className="flex justify-between gap-x-6 py-5">
          <div className="flex min-w-0 gap-x-4">            
            <div className="min-w-0 flex-auto">
              <p className="text-sm font-semibold leading-6 text-gray-900">
              <span className="inline-flex items-center rounded-md bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20">
              {tipoItemsName(+operation.tipoItem)}
              </span>
               
              </p>
              <p className="mt-1 truncate text-xs leading-5 text-gray-500">Motivo: {operation.motivoPrestamo}</p>
            </div>
          </div>
          <div className="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
            <p className="text-sm leading-6 text-gray-900">Total prestamo: <span className="ml-3 text-sm font-medium text-indigo-600 hover:text-indigo-500">${operation.cantidad}</span></p>
            <div>
            <button
            onClick={() => dispatch({type: 'set-active-operation', payload: {id: operation.id}})}
            >
                <PencilSquareIcon
                className="h-6 w-6 text-gray-400"
                
                />
            </button>  
            <button
            onClick={() => dispatch({type: 'delete-operation', payload: {id: operation.id}})}
            >
                <XCircleIcon
                className="h-6 w-6 text-red-500"                
                />
            </button> 
            </div>         
          </div>
        </li>
      ))}
    </ul>
        </div>
        
      </div>
    </div>
       </>
    )
}