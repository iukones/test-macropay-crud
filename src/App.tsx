import { useReducer, useEffect } from 'react'
import Form from "./components/Form"
import { operationReducer, initialState } from './reducers/operation-reducer'
import OperationList from './components/OperationList'


function App() {
  
  const [state, dispatch] = useReducer(operationReducer, initialState)
  //console.log(state)

  useEffect(() => {
    localStorage.setItem('operations', JSON.stringify(state.operations))
  }, [state.operations])

  return (
    <>
      <header className="bg-blue-600">
        <div className="max-4xl mx-auto flex justify-between">
        <img 
        src="https://macropay.mx/wp-content/uploads/2022/10/Logotipo-Macropay.svg" 
        className="h-25 w-50 p-5" 
        alt=""
        ></img>
          <h1 className="text-2xl font-bold tracking-tight text-white sm:text-2xl p-4">
            Alta de prestamos
          </h1>
        </div>
      </header>

      <section className="bg-blue-400 py-20 px-5">
        <div className="max-w-4xl mx-auto">
          <Form 
          dispatch={dispatch}
          state= {state}
          />
        </div>
      </section>

      <section className='p-10 mx-auto max-w-6xl'>
        
        <OperationList 
          operations={state.operations}
          dispatch={dispatch}
        />

      </section>
    </>
  )
}

export default App
