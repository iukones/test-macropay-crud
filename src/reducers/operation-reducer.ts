import { Operation } from "../types";


export type OperationActions = 
{ type: 'safe-operation', payload: { newOperation : Operation } } |
{ type: 'set-active-operation', payload: { id: Operation['id'] } } |
{ type: 'delete-operation', payload: { id: Operation['id'] } }


export type OperationState = {
    operations: Operation[],
    activeId: Operation['id']
}

const localStorageOperations = () : Operation[] => {
    const operations = localStorage.getItem('operations');
    return operations ? JSON.parse(operations) : []   

}

export const initialState: OperationState = {
    operations: localStorageOperations(),
    activeId: ''
}

export const operationReducer = (
    state: OperationState = initialState,
    action: OperationActions
) => {

    if (action.type === 'safe-operation') {
        //aquí manejamos toda la lógica para manejar el estado
        //console.log(action.payload.newOperation);
        let updateOperation : Operation[] = []

        if (state.activeId) {
            updateOperation = state.operations.map(operation => operation.id === state.activeId ? action.payload.newOperation : operation)
            
        }else{
            updateOperation = [...state.operations, action.payload.newOperation]

        }

        return {
            ...state,
            operations: updateOperation,
            activeId: ''
        }
        
        
    }

    if (action.type === 'set-active-operation') {
        return {
            ...state,
            activeId: action.payload.id
        }
    }

    if (action.type === 'delete-operation') {
        return {
            ...state,
            operation: state.operations.filter(operations => operations.id !== action.payload.id)
        }
    }
    return state
}


