export type TipoItems = {
    id: number,
    nombre: string
}

export type Operation = {
    id: string,    
    tipoItem: number,
    motivoPrestamo: string,
    cantidad: number,   

}